output "subnets" {
  value       = module.subnets.subnets
  description = "The created subnet resources"
}
